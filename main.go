package main

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/bartwillems/biersfuif/pkg/api"
	"gitlab.com/bartwillems/biersfuif/pkg/config"
	"gitlab.com/bartwillems/biersfuif/pkg/db"
)

func main() {

	config.Load()

	err := db.Connect(config.Settings.Postgres)

	if err != nil {
		log.Fatalf("unable to connecto to the database %v", err)
	}

	defer db.Con.Close()

	log.Info("ok")

	db.Migrate()

	api.Launch()
}
