package models

import "github.com/jinzhu/gorm"

// - create event
// - configure max amount of teams & beverages per team
// - add teams
// - each team adds their beverages to their slots
// - teams start drinking
// - prices of the beverages fluctuate
//   - beverage of slot 0 is bought => slots 1..7 go up in price
//   - eg: UPDATE prices SET price = math.floor(price * 1.10) WHERE slot == 0 and price drop for other blablabla;

const ScalePercentage float32 = 0.05

// how does this even work
// const CrashFactor

// dev only, real data manipulation should happen in the database with locks
func increaseFactor(factor float32) float32 {
	return factor * (1 + ScalePercentage)
}

func decreaseFactor(factor float32) float32 {
	return factor * (1 - ScalePercentage)
}

// Event is an actual beursfuif event with n amount of teams
type Event struct {
	gorm.Model
	Teams         []Team
	TeamLimit     int `gorm:"not null"`
	BeverageLimit int
	Fluctuations  []Fluctuation `gorm:"foreignkey:EventID"`
}

// Fluctuation is an entry for a beverage with a price fluctuation factor
type Fluctuation struct {
	gorm.Model
	EventID uint
	Factor  float32 `gorm:"not null"`
}

// Beverage is a slot entry with a name and price config (in cents)
type Beverage struct {
	gorm.Model
	FluctuationID uint
	Fluctuation   Fluctuation
	TeamID        uint
	Name          string
	MinimumPrice  int
	MaximumPrice  int
	DefaultPrice  int
}

// Team is a group of people participating in the event
type Team struct {
	gorm.Model
	Name      string
	Beverages []Beverage
}

type sale struct {
	ID         int64
	TeamID     int64
	BeverageID int64
	Price      int
}
