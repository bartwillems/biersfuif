package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	ginlogrus "github.com/toorop/gin-logrus"
	"gitlab.com/bartwillems/biersfuif/pkg/config"
)

var router *gin.Engine

// Launch starts the webserver
func Launch() error {
	if config.Settings.Environment == "production" {
		gin.SetMode(gin.ReleaseMode)
	}

	router = gin.New()
	router.Use(ginlogrus.Logger(log.StandardLogger()), gin.Recovery())

	initializeRoutes()

	return router.Run()
}

func initializeRoutes() {
	router.GET("/beverages", getBeverages)
}

func getBeverages(c *gin.Context) {
	c.String(http.StatusOK, "ok")
}
