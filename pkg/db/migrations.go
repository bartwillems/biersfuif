package db

import (
	"errors"

	"gitlab.com/bartwillems/biersfuif/pkg/config"
	"gitlab.com/bartwillems/biersfuif/pkg/models"
)

// Migrate runs the database migrations
func Migrate() {
	// Conn.
	Con.AutoMigrate(&models.Fluctuation{}, &models.Event{})
	// return errors.New("NotImplemented")
}

// Rollback resets the last migration
func Rollback() error {
	return errors.New("NotImplemented")
}

// Reset clears the whole database, only during in development
func Reset() error {
	if config.Settings.Environment == "production" {
		return errors.New("not available during production")
	}

	return errors.New("NotImplemented")
}
