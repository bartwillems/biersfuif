package db

import (
	"fmt"

	"github.com/jinzhu/gorm"
	// silence the warning!
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/bartwillems/biersfuif/pkg/config"
)

// Con is the database connection
var Con *gorm.DB

// Connect connects to the database duh
func Connect(config config.Postgres) error {
	var err error
	Con, err = gorm.Open("postgres", fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", config.Host, config.Port, config.Username, config.Password, config.Database))

	return err

}
