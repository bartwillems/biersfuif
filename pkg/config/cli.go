package config

import "github.com/mkideal/cli"

// ArgT holds the CLI interface configuration
type ArgT struct {
	cli.Helper
	Migrate  bool `cli:"migrate" usage:"run the database migrations"`
	Rollback bool `cli:"migrate" usage:"reset the last migration"`
	Reset    bool `cli:"migrate" usage:"clear the whole database, only available during in development"`
}
