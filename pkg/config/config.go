package config

import (
	"os"
	"strings"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
)

type Config struct {
	Environment string `default:"production"`
	LogLevel    string `default:"info"`

	Postgres Postgres
}

// Postgres is the postgres configuration struct
type Postgres struct {
	Username string `required:"true"`
	Password string `required:"true"`
	Database string `required:"true"`
	Host     string `required:"true"`
	Port     int    `required:"true"`
}

// Settings is the runtime config struct
var Settings Config

// Load the config
func Load() {
	godotenv.Load()
	err := envconfig.Process("", &Settings)

	if err != nil {
		log.Fatalf("unable to load the config: %v", err)
	}

	setLogLevel(Settings.LogLevel)
	log.SetOutput(os.Stdout)
}

func setLogLevel(level string) {
	switch strings.ToLower(level) {
	case "trace":
		log.SetLevel(log.TraceLevel)
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	case "fatal":
		log.SetLevel(log.FatalLevel)
	case "panic":
		log.SetLevel(log.PanicLevel)
	default:
		log.SetLevel(log.InfoLevel)
		log.Warnf("invalid loglevel'%s', falling back to info")
	}
}
