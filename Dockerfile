FROM golang:1.13 as builder

ENV GO111MODULE=on
ENV dink=dink

WORKDIR /build

COPY go.mod go.sum ./

RUN go mod download

COPY main.go .
COPY pkg ./pkg

RUN CGO_ENABLED=0 go build -o biersfuif

FROM alpine:3.11

EXPOSE 8080
CMD [ "/app/biersfuif" ]

RUN apk --no-cache add ca-certificates

COPY --from=builder /build/biersfuif /app/biersfuif

USER guest
