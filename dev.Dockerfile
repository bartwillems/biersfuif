FROM golang:1.13-alpine

EXPOSE 8080

RUN apk add --no-cache bash git openssh

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY main.go .
COPY pkg ./pkg

CMD ["go", "run", "."]