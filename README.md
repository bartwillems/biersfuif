# beursfuif

This is an open source implementation of the beursfuif concept.

## status

Beursfuif is currently under development, but still very much in the PoC phase

## dev updates

## 2020-01-30

### GORM

I will be looking for an alternative because:

1. it doesn't seem to create foreign key relations when automigrating.
1. it has an inconvenient db connection mechanism (only a string) which is erro prone
1. it seems to really lack documentation and the existing documentation is often unclear
1. migrations don't return errors, they just panic
1. for some reason, the gorm model base definition is uint in stead of int64
