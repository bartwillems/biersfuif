module gitlab.com/bartwillems/biersfuif

go 1.13

require (
	github.com/Bowery/prompt v0.0.0-20190916142128-fa8279994f75 // indirect
	github.com/gin-gonic/gin v1.5.0
	github.com/jinzhu/gorm v1.9.12
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mkideal/cli v0.0.3
	github.com/mkideal/pkg v0.0.0-20170503154153-3e188c9e7ecc // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/toorop/gin-logrus v0.0.0-20190701131413-6c374ad36b67
)
